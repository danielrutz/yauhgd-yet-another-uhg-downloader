#include <QDebug>
#include <QApplication>

#include "uhgprepcheckinfoswizardpage.h"
#include "uhgprepwizard.h"

UHGPrepCheckInfosWizardPage::UHGPrepCheckInfosWizardPage(QWidget *parent) :
    QWizardPage(parent)
{
    text = new QLabel(tr("YAUHGD wird nach diesem Dialog mit der Bereitstellung der Daten beginnen.\n"
                          "Bitte überprüfen Sie aus Sicherheitsgründen nochmals, ob folgende Angaben korrekt sind:"));
    layout = new QVBoxLayout(this);
    layout->addWidget(text);
}

void UHGPrepCheckInfosWizardPage::initializePage()
{
    QString versionString(tr("Version der Wii: %1.%2").arg(static_cast<UHGPrepWizard*>(wizard())->getMajorVersion()).arg(static_cast<UHGPrepWizard*>(wizard())->getMinorVersion()));
    switch(static_cast<UHGPrepWizard*>(wizard())->getWiiRegion())
    {

    case UHGPrepWizard::Region::Region_PAL:
        versionString += "E";
        break;
    case UHGPrepWizard::Region::Region_JPN:
        versionString += "J";
        break;
    case UHGPrepWizard::Region::Region_USA:
        versionString += "U";
        break;
    case UHGPrepWizard::Region::Region_KOR:
        versionString += "K";
        break;
    default:
        qDebug() << "FAIL: Region irgendwie nicht existent! Fehler in CheckInfosWizardPage::initializePage()";
        qApp->exit(-1);
    }

    version = new QLabel(versionString);
    layout->addWidget(version);

    QString exploitString("Verwendeter Exploit: ");

    switch(static_cast<UHGPrepWizard*>(wizard())->getExploit())
    {
    case UHGPrepWizard::Exploit::Exploit_Bannerbomb1:
        exploitString += tr("Bannerbomb v108");
        break;
    case UHGPrepWizard::Exploit::Exploit_Bannerbomb2:
        exploitString += tr("Bannerbomb v200");
        break;
    case UHGPrepWizard::Exploit::Exploit_Batthaxx:
        exploitString += tr("Batthaxx");
        break;
    case UHGPrepWizard::Exploit::Exploit_EriHaKawai:
        exploitString += tr("Eri HaKawai");
        break;
    case UHGPrepWizard::Exploit::Exploit_IndianaPWNS:
        exploitString += tr("Indiana PWNS");
        break;
    case UHGPrepWizard::Exploit::Exploit_Letterbomb:
        exploitString += tr("Letterbomb");
        break;
    case UHGPrepWizard::Exploit::Exploit_ReturnJODI:
        exploitString += tr("Return of the JODI");
        break;
    case UHGPrepWizard::Exploit::Exploit_SmashStack:
        exploitString += tr("SmashStack");
        break;
    case UHGPrepWizard::Exploit::Exploit_TwilightHack:
        exploitString += tr("Twilight Hack");
        break;
    case UHGPrepWizard::Exploit::Exploit_YuGiOWNED:
        exploitString += tr("YuGiOWNED");
        break;
    default:
        qDebug() << "FAIL: Exploit irgendwie nicht existent! Fehler in CheckInfosWizardPage::initializePage()";
        qApp->exit(-1);
    }

    exploit = new QLabel(exploitString);
    layout->addWidget(exploit);
}

void UHGPrepCheckInfosWizardPage::cleanupPage()
{
    QWizardPage::cleanupPage();
    layout->removeWidget(exploit);
    layout->removeWidget(version);
    delete exploit;
    exploit = nullptr;
    delete version;
    version = nullptr;
}
