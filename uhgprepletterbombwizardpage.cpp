#include "uhgprepletterbombwizardpage.h"
#include <QApplication>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include "uhgprepwizard.h"


UHGPrepLetterbombWizardPage::UHGPrepLetterbombWizardPage(QWidget *parent) :
    QWizardPage(parent)
{
    text = new QLabel(tr("Es sollte sich nun ihr Webbrowser öffnen.\n"
                         "Sollte dies nicht geschehen, öffnen Sie ihn bitte selbst und gehen auf die Website \"http://please.hackmii.com\".\n"
                         "Geben Sie dort alle Informationen ein und entfernen Sie den Haken bei \"Bundle the Hackmii Installer for me!\".\n"
                         "Klicken Sie danach auf \"Cut the red wire\" und laden Sie die Datei an einen Ort herunter, wo Sie sie wiederfinden.\n"
                         "Drücken Sie dann auf \"Datei öffnen\" in diesem Assistenten und geben Sie den Dateiordner an.\n"
                         "Sollten Sie einen Spieleexploit bevorzugen, so drücken Sie \"Ich nutze lieber ein Spiel\"."
                         ));
    layout = new QVBoxLayout;
    OpenFileButton = new QPushButton(tr("Datei öffnen"));
    BetterUseGameExploitButton = new QPushButton(tr("Ich nutze lieber ein Spiel."));
    LinkToLetterbomb = new QLineEdit();
    LinkToLetterbomb->setEnabled(false);
    connect(OpenFileButton, SIGNAL(clicked()), this, SLOT(ButtonClick()));
    connect(BetterUseGameExploitButton, SIGNAL(clicked()), this, SLOT(GoToGameExploitPage()));

    layout->addWidget(text);
    layout->addWidget(OpenFileButton);
    layout->addWidget(LinkToLetterbomb);
    layout->addWidget(BetterUseGameExploitButton);
    setLayout(layout);
}

void UHGPrepLetterbombWizardPage::initializePage()
{
    QDesktopServices::openUrl(QUrl("http://please.hackmii.com/"));
    clickedToGameExploit = false;
}

void UHGPrepLetterbombWizardPage::ButtonClick()
{
    LinkToLetterbomb->setText(QFileDialog::getOpenFileName(this, tr("Bitte Letterbombdatei auswählen"), QString(), tr("Letterbomb.zip (Letterbomb*.zip)")));
}

bool UHGPrepLetterbombWizardPage::validatePage()
{
    if(!clickedToGameExploit) {
    QFile test(LinkToLetterbomb->text());
    if (test.exists())
    {
        static_cast<UHGPrepWizard*>(wizard())->setExploit(UHGPrepWizard::Exploit::Exploit_Letterbomb);
        return true;
    }
    else
    {
        QMessageBox::critical(this, tr("Datei nicht gefunden"), tr("Die Datei wurde nicht gefunden.\nBitte überprüfen Sie den Pfad."));
        return false;
    }

    }
    else return true;
}

void UHGPrepLetterbombWizardPage::GoToGameExploitPage()
{
    clickedToGameExploit = true;
    wizard()->next();
}

int UHGPrepLetterbombWizardPage::nextId() const
{
    if (clickedToGameExploit)
        return UHGPrepWizard::Page_Exploits;
    else
        return UHGPrepWizard::Page_CheckInfos;
}
