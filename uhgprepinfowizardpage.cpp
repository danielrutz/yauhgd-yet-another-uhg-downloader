/*
    Copyright (C) 2012-2014 Daniel Rutz
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    
    Dieses Programm ist freie Software. Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation
    veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß
    Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
    
    Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
    dass es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE,
    sogar ohne die implizite Garantie der MARKTREIFE oder der
    VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der
    GNU General Public License.
    
    Sie sollten ein Exemplar der GNU General Public License zusammen mit
    diesem Programm erhalten haben. Falls nicht, siehe
    <http://www.gnu.org/licenses/>.
    
*/

#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "uhgprepinfowizardpage.h"
#include "uhgprepwizard.h"

UHGPrepInfoWizardPage::UHGPrepInfoWizardPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(tr("Informationen über die bereitgestellte Software"));
    layout = new QVBoxLayout;
    verLayout = new QHBoxLayout;

    tableIntroduction = new QLabel(tr("Versionen der integrierten Apps:"));
    apps = new QLabel(tr("Hermes cIOS\nd2x cIOS\ncfg USBLoader\nGecko OS\nHackmii Installer\nNeogamma\nPriloader German Mod\nSIP\nsysCheck\nUSB-Loader GX\nWiiFlow\nYAWMM_de"));
    versions = new QLabel(tr("Test"));

    verLayout->addWidget(apps);
    verLayout->addWidget(versions);
    layout->addWidget(tableIntroduction);
    layout->addLayout(verLayout);
    setLayout(layout);



}


int UHGPrepInfoWizardPage::nextId() const
{
    return UHGPrepWizard::Page_VersionSelect;
}
