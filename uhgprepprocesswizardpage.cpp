#include <QDir>
#include <QMessageBox>
#include <QFile>

#include "jpatch.h"

#include "uhgprepprocesswizardpage.h"
#include "uhgprepwizard.h"

//Diese Defines sorgen dafür, dass man von keine nichtssagenden Zahlen mehr im Code hat, mal ganz im Ernst, von diesen Literalen wird einem schlecht
#define BC_TID    Q_UINT64_C(0x0000000100000100)
#define IOS4_TID  Q_UINT64_C(0x0000000100000004)
#define IOS9_TID  Q_UINT64_C(0x0000000100000009)
#define IOS10_TID Q_UINT64_C(0x000000010000000A)
#define IOS12_TID Q_UINT64_C(0x000000010000000C)
#define IOS13_TID Q_UINT64_C(0x000000010000000D)
#define IOS14_TID Q_UINT64_C(0x000000010000000E)
#define IOS15_TID Q_UINT64_C(0x000000010000000F)
#define IOS16_TID Q_UINT64_C(0x0000000100000010)
#define IOS17_TID Q_UINT64_C(0x0000000100000011)
#define IOS21_TID Q_UINT64_C(0x0000000100000012)
#define IOS22_TID Q_UINT64_C(0x0000000100000013)
#define IOS28_TID Q_UINT64_C(0x000000010000001C)
#define IOS30_TID Q_UINT64_C(0x000000010000001E)
#define IOS31_TID Q_UINT64_C(0x000000010000001F)
#define IOS33_TID Q_UINT64_C(0x0000000100000021)
#define IOS34_TID Q_UINT64_C(0x0000000100000022)
#define IOS35_TID Q_UINT64_C(0x0000000100000023)
#define IOS36_TID Q_UINT64_C(0x0000000100000024)
#define IOS37_TID Q_UINT64_C(0x0000000100000025)
#define IOS38_TID Q_UINT64_C(0x0000000100000026)
#define IOS41_TID Q_UINT64_C(0x0000000100000029)
#define IOS43_TID Q_UINT64_C(0x000000010000002B)
#define IOS45_TID Q_UINT64_C(0x000000010000002D)
#define IOS46_TID Q_UINT64_C(0x000000010000002E)
#define IOS48_TID Q_UINT64_C(0x0000000100000030)
#define IOS50_TID Q_UINT64_C(0x0000000100000032)
#define IOS51_TID Q_UINT64_C(0x0000000100000033)
#define IOS52_TID Q_UINT64_C(0x0000000100000034)
#define IOS53_TID Q_UINT64_C(0x0000000100000035)
#define IOS55_TID Q_UINT64_C(0x0000000100000037)
#define IOS56_TID Q_UINT64_C(0x0000000100000038)
#define IOS57_TID Q_UINT64_C(0x0000000100000039)
#define IOS58_TID Q_UINT64_C(0x000000010000003A)
#define IOS60_TID Q_UINT64_C(0x000000010000003C)
#define IOS61_TID Q_UINT64_C(0x000000010000003D)
#define IOS62_TID Q_UINT64_C(0x000000010000003E)
#define IOS70_TID Q_UINT64_C(0x0000000100000046)
#define IOS80_TID Q_UINT64_C(0x0000000100000050)
#define MIOS_TID  Q_UINT64_C(0x0000000100000101)
#define SHOPK_TID Q_UINT64_C(0x000100024841424B) //K steht für Koreanisch
#define SHOP_TID  Q_UINT64_C(0x0001000248414241)


UHGPrepProcessWizardPage::UHGPrepProcessWizardPage(QWidget *parent) :
    QWizardPage(parent), nusloader(this)
{
    layout = new QVBoxLayout();
    progressbar = new QProgressBar();
    text = new QLabel(tr("Bitte warten Sie, YAUHGD lädt nun die Daten herunter.\nDieser Vorgang kann bis zu 30 Minuten dauern."));

    layout->addWidget(text);
    layout->addWidget(progressbar);

    setLayout(layout);

    connect(&nusloader, SIGNAL(SendTotalProgress(int)), this, SLOT(DownloadProgressChanged(int)));
    connect(&nusloader, SIGNAL(SendData(NusJob)), this, SLOT(DownloadWADFinished(NusJob)));
    connect(&nusloader, SIGNAL(SendDone()), this, SLOT(DownloadAllWADFinished()));
    connect(&nusloader, SIGNAL(SendError(QString,NusJob)), this, SLOT(DownloadWADFailed(QString,NusJob)));
}

void UHGPrepProcessWizardPage::initializePage()
{
    UHGPrepWizard* wiz = static_cast<UHGPrepWizard*>(wizard());
    //Erzeuge NusJobs
    for(int i = 0; i < 36; i++) //36 IOS bzw. Kanäle müssen immer geladen werden
    {
        NusJob tempJob;
        tempJob.decrypt = false;
        switch(i)
        {
        case 0: //BCv6
            tempJob.tid = BC_TID;
            tempJob.version = 6;
            break;

        case 1: //IOS4v65280
            tempJob.tid = IOS4_TID;
            tempJob.version = 65280;
            break;

        case 2: //IOS9v1034
            tempJob.tid = IOS9_TID;
            tempJob.version = 1034;
            break;

        case 3: //IOS10v768
            tempJob.tid = IOS10_TID;
            tempJob.version = 768;
            break;

        case 4: //IOS12v526
            tempJob.tid = IOS12_TID;
            tempJob.version = 526;
            break;

        case 5: //IOS13v1032
            tempJob.tid = IOS13_TID;
            tempJob.version = 1032;
            break;

        case 6: //IOS14v1032
            tempJob.tid = IOS14_TID;
            tempJob.version = 1032;
            break;

        case 7: //IOS15v1032
            tempJob.tid = IOS15_TID;
            tempJob.version = 1032;
            break;

        case 8: //IOS16v512
            tempJob.tid = IOS16_TID;
            tempJob.version = 512;
            break;

        case 9: //IOS17v1032
            tempJob.tid = IOS17_TID;
            tempJob.version = 1032;
            break;

        case 10: //IOS21v1093
            tempJob.tid = IOS21_TID;
            tempJob.version = 1093;
            break;

        case 11: //IOS22v1294
            tempJob.tid = IOS22_TID;
            tempJob.version = 1294;
            break;

        case 12: //IOS28v1807
            tempJob.tid = IOS28_TID;
            tempJob.version = 1807;
            break;

        case 13: //IOS31v3608
            tempJob.tid = IOS31_TID;
            tempJob.version = 3608;
            break;

        case 14: //IOS33v3608
            tempJob.tid = IOS33_TID;
            tempJob.version = 3608;
            break;

        case 15: //IOS34v3608
            tempJob.tid = IOS34_TID;
            tempJob.version = 3608;
            break;

        case 16: //IOS35v3608
            tempJob.tid = IOS35_TID;
            tempJob.version = 3608;
            break;

        case 17: //IOS36v3608
            tempJob.tid = IOS36_TID;
            tempJob.version = 3608;
            break;

        case 18: //IOS37v5663
            tempJob.tid = IOS37_TID;
            tempJob.version = 5663;
            break;

        case 19: //IOS38v4142
            tempJob.tid = IOS38_TID;
            tempJob.version = 4142;
            break;

        case 20: //IOS41v3607
            tempJob.tid = IOS41_TID;
            tempJob.version = 3607;
            break;

        case 21: //IOS43v3607
            tempJob.tid = IOS43_TID;
            tempJob.version = 3607;
            break;

        case 22: //IOS45v3607
            tempJob.tid = IOS45_TID;
            tempJob.version = 3607;
            break;

        case 23: //IOS46v3607
            tempJob.tid = IOS46_TID;
            tempJob.version = 3607;
            break;

        case 24: //IOS48v4124
            tempJob.tid = IOS48_TID;
            tempJob.version = 4124;
            break;

        case 25: //IOS51v4864
            tempJob.tid = IOS51_TID;
            tempJob.version = 4864;
            break;

        case 26: //IOS52v5888
            tempJob.tid = IOS52_TID;
            tempJob.version = 5888;
            break;

        case 27: //IOS53v5663
            tempJob.tid = IOS53_TID;
            tempJob.version = 5663;
            break;

        case 28: //IOS55v5663
            tempJob.tid = IOS55_TID;
            tempJob.version = 5663;
            break;

        case 29: //IOS56v5662
            tempJob.tid = IOS56_TID;
            tempJob.version = 5662;
            break;

        case 30: //IOS57v5919
            tempJob.tid = IOS57_TID;
            tempJob.version = 5919;
            break;

        case 31: //IOS58v6176
            tempJob.tid = IOS58_TID;
            tempJob.version = 6176;
            break;

        case 32: //IOS61v5662
            tempJob.tid = IOS61_TID;
            tempJob.version = 5662;
            break;

        case 33: //IOS62v6430
            tempJob.tid = IOS62_TID;
            tempJob.version = 6430;
            break;

        case 34: //MIOSv10
            tempJob.tid = MIOS_TID;
            tempJob.version = 10;
            break;

        case 35: //ShoppingChannelv21
            if (wiz->getWiiRegion() == UHGPrepWizard::Region::Region_KOR)
                tempJob.tid = SHOPK_TID;
            else tempJob.tid = SHOP_TID;
            tempJob.version = 21;
            break;
        }
        list.append(tempJob);

        NusJob systemios;
        //Lade System-IOS, hierbei wird nur System >=3.3 betrachtet, da darunter der Trucha Bug noch nicht gefixt war und das System-IOS nur zum Trucha Bug patchen benötigt wird
        if (wiz->getMajorVersion() == 3)
        {
            if(wiz->getMinorVersion() == 3 && wiz->getWiiRegion() != UHGPrepWizard::Region::Region_KOR)//IOS30v2576
            {
                systemios.tid = IOS30_TID;
                systemios.version = 2576;
            }
            else if (wiz->getMinorVersion() == 4) //IOS50v4889
            {
                systemios.tid = IOS50_TID;
                systemios.version = 4889;
            }
        }
        else if (wiz->getMajorVersion() == 4)
        {
            if(wiz->getMinorVersion() == 0 || wiz->getMinorVersion() == 1) //IOS60v6174
            {
                systemios.tid = IOS60_TID;
                systemios.version = 6174;
            }
            else if(wiz->getMinorVersion() == 2) //IOS70v6687
            {
                systemios.tid = IOS70_TID;
                systemios.version = 6687;
            }
            else if(wiz->getMinorVersion() == 3) //IOS80v6944
            {
                systemios.tid = IOS80_TID;
                systemios.version = 6944;
            }
        }
        list.append(systemios);
        nusloader.GetTitles(list); //Starte IOS-Download

    }
}

void UHGPrepProcessWizardPage::DownloadWADFailed(const QString& error, const NusJob& failedJob)
{
    //Verbesserungswürdig...
    QMessageBox::critical(this, tr("Fehler beim Laden eines IOS"), tr("Beim Laden eines IOS ist ein Fehler aufgetreten."));
    qApp->quit();
}

void UHGPrepProcessWizardPage::DownloadProgressChanged(int progress)
{
    progressbar->setValue(progress);
}

void UHGPrepProcessWizardPage::DownloadWADFinished(const NusJob &finishedJob)
{
    //STUB
}

void UHGPrepProcessWizardPage::DownloadAllWADFinished()
{
    //STUB
    finished = true;
    emit completeChanged();
}

bool UHGPrepProcessWizardPage::isComplete() const
{
    if (finished)
        return true;
    else return false;
}
