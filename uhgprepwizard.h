/*
    Copyright (C) 2012-2014 Daniel Rutz
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    
    Dieses Programm ist freie Software. Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation
    veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß
    Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
    
    Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
    dass es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE,
    sogar ohne die implizite Garantie der MARKTREIFE oder der
    VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der
    GNU General Public License.
    
    Sie sollten ein Exemplar der GNU General Public License zusammen mit
    diesem Programm erhalten haben. Falls nicht, siehe
    <http://www.gnu.org/licenses/>.
    
*/

#ifndef UHGPREPWIZARD_H
#define UHGPREPWIZARD_H

#include <QWizardPage>
#include <QWizard>
#include "uhgprepstartwizardpage.h"
#include "uhgprepinfowizardpage.h"
#include "uhgchooseversionwizardpage.h"
#include "uhgprepletterbombwizardpage.h"
#include "uhgprepgameexploitwizardpage.h"
#include "uhgprepcheckinfoswizardpage.h"
#include "uhgprepprocesswizardpage.h"

class UHGPrepWizard : public QWizard
{
    Q_OBJECT
public:

    enum class Region{
        Region_PAL,
        Region_JPN,
        Region_USA,
        Region_KOR
    };

    enum class Exploit{
        Exploit_Letterbomb,
        Exploit_TwilightHack,
        Exploit_Bannerbomb1,
        Exploit_Bannerbomb2,
        Exploit_ReturnJODI,
        Exploit_IndianaPWNS,
        Exploit_YuGiOWNED,
        Exploit_SmashStack,
        Exploit_EriHaKawai,
        Exploit_Batthaxx
    };


    explicit UHGPrepWizard(QWidget *parent = 0);
    inline void setHasVWii(bool value = true) {hasVWii = value;}
    inline void setVersion(int MajorVersion, int MinorVersion, Region region) {MajorWiiVersion = MajorVersion; MinorWiiVersion = MinorVersion; WiiRegion = region;}
    inline int getMajorVersion() {return MajorWiiVersion;}
    inline int getMinorVersion() {return MinorWiiVersion;}
    inline Region getWiiRegion() {return WiiRegion;}
    inline void setExploit(Exploit newExploit) {UsedExploit = newExploit;}
    inline Exploit getExploit() {return UsedExploit;}

    //Enumeration für interne Seitennummern
    enum{
        Page_Start,
        Page_Info,
        Page_VersionSelect,
        Page_Wilbrand,
        Page_Exploits,
        Page_Letterbomb,
        Page_CheckInfos,
        Page_Working,
        Page_Finished
    };



signals:

public slots:

private:
    bool hasVWii = false;
    int MajorWiiVersion;
    int MinorWiiVersion;
    Region WiiRegion;
    Exploit UsedExploit;


};

#endif // UHGPREPWIZARD_H
