#ifndef UHGPREPPROCESSWIZARDPAGE_H
#define UHGPREPPROCESSWIZARDPAGE_H

#include <QWizardPage>
#include <QVBoxLayout>
#include <QLabel>
#include <QList>
#include <QProgressBar>

#include "wiiqt/tools.h"
#include "wiiqt/nusdownloader.h"
#include "wiiqt/wad.h"

class UHGPrepProcessWizardPage : public QWizardPage
{
    Q_OBJECT


private:
    QVBoxLayout* layout = nullptr;
    QLabel* text = nullptr;
    QProgressBar* progressbar = nullptr;
    QList<NusJob> list;
    NusDownloader nusloader;
    bool finished = false;

public:
    explicit UHGPrepProcessWizardPage(QWidget *parent = 0);
    void initializePage() override;
    bool isComplete() const override;

signals:

public slots:
   void DownloadWADFinished(const NusJob& finishedJob); //packt Daten in WADs und patcht wenn nötig die IOS
   void DownloadProgressChanged(int progress); //ändert angezeigten Fortschritt
   void DownloadAllWADFinished(); //beginnt mit Sachen nachdem WADs fertiggeladen sind.
   void DownloadWADFailed(const QString& error, const NusJob& failedJob); //Fehler bei Download

};

#endif // UHGPREPPROCESSWIZARDPAGE_H
