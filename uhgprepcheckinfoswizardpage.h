#ifndef UHGPREPCHECKINFOSWIZARDPAGE_H
#define UHGPREPCHECKINFOSWIZARDPAGE_H

#include <QLabel>
#include <QString>
#include <QVBoxLayout>
#include <QWizardPage>

class UHGPrepCheckInfosWizardPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit UHGPrepCheckInfosWizardPage(QWidget *parent = 0);
    void initializePage() override;
    void cleanupPage() override;
signals:

public slots:

private:
    QLabel* text = nullptr;
    QVBoxLayout* layout = nullptr;
    QLabel* version = nullptr;
    QLabel* exploit = nullptr;
};

#endif // UHGPREPCHECKINFOSWIZARDPAGE_H
