#ifndef UHGPREPLETTERBOMBWIZARDPAGE_H
#define UHGPREPLETTERBOMBWIZARDPAGE_H

#include <QWizardPage>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>

class UHGPrepLetterbombWizardPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit UHGPrepLetterbombWizardPage(QWidget *parent = 0);
    void initializePage() override;
    bool validatePage() override;
    int nextId() const override;

public slots:
        void ButtonClick();
        void GoToGameExploitPage();

private:

    bool clickedToGameExploit;
    QVBoxLayout* layout = nullptr;
    QLabel* text = nullptr;
    QLineEdit* LinkToLetterbomb = nullptr;
    QPushButton* OpenFileButton = nullptr;
    QPushButton* BetterUseGameExploitButton = nullptr;
};

#endif // UHGPREPLETTERBOMBWIZARDPAGE_H
