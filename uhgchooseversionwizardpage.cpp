/*
    Copyright (C) 2012-2014 Daniel Rutz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Dieses Programm ist freie Software. Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation
    veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß
    Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.

    Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
    dass es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE,
    sogar ohne die implizite Garantie der MARKTREIFE oder der
    VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der
    GNU General Public License.

    Sie sollten ein Exemplar der GNU General Public License zusammen mit
    diesem Programm erhalten haben. Falls nicht, siehe
    <http://www.gnu.org/licenses/>.

*/
#include <QMessageBox>
#include <QApplication>
#include <QChar>

#include "uhgprepwizard.h"
#include "uhgchooseversionwizardpage.h"

UHGChooseVersionWizardPage::UHGChooseVersionWizardPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(tr("Eingabe der Version des Wii-Menüs"));
    layout = new QVBoxLayout;
    text = new QLabel(tr("Bitte geben Sie die Versionsnummer Ihrer Wii ein (Beispiel: 4.1E)."));
    versionInput = new QLineEdit();
    versionInput->setInputMask("9.9>A");
    versionInput->setFixedWidth(50);
    registerField("version", versionInput);
    vWiiButton = new QPushButton(tr("Ich habe eine Wii U!"));
    vWiiButton->setFixedWidth(225);
    connect(vWiiButton, SIGNAL(clicked()), this, SLOT(onvWiiButtonClick()));
    layout->addWidget(text);
    layout->addWidget(versionInput);
    layout->addWidget(vWiiButton);
    setLayout(layout);
}

void UHGChooseVersionWizardPage::onvWiiButtonClick()
{
    static_cast<UHGPrepWizard*>(wizard())->setHasVWii();
    QMessageBox::critical(this, tr("Nicht mit Wii U kompatibel!"), tr("Leider ist dieses Programm (noch) nicht mit der Wii U kompatibel.\nDer Wizard bricht nun ab..."));
    wizard()->close();
}

bool UHGChooseVersionWizardPage::validatePage()
{
    int MajorVersion = (field("version").toString())[0].digitValue();
    int MinorVersion = (field("version").toString())[2].digitValue();
    QChar regionChar = (field("version").toString())[3];
    UHGPrepWizard::Region region;

    if (regionChar == QChar('E'))
        region = UHGPrepWizard::Region::Region_PAL;
    else if (regionChar == QChar('U'))
        region = UHGPrepWizard::Region::Region_USA;
    else if (regionChar == QChar('J'))
        region = UHGPrepWizard::Region::Region_JPN;
    else if (regionChar == QChar('K'))
        region = UHGPrepWizard::Region::Region_KOR;
    else
    {
        QMessageBox::critical(this, tr("Fehler bei Versionseingabe!"), tr("Leider ist Ihre Region ungültig oder unbekannt und nicht unterstützt.\n"
                                                                          "Bitte prüfen Sie Ihre Eingabe und informieren Sie im Zweifelsfall den Entwickler."));
        return false;
    }

    if ((MajorVersion < 1) || //Version kleiner 1.0
        (MinorVersion < 0) || //Version kleiner als X.0
        (MajorVersion == 1 && MinorVersion != 0) || //es gibt nur 1.0
        (MajorVersion == 2 && MinorVersion  > 2) || //es gibt nur 2.0-2.2
        (MajorVersion == 3 && MinorVersion  > 5) || //es gibt nur 3.0-3.5
        (MajorVersion == 3 && MinorVersion == 5 && region != UHGPrepWizard::Region::Region_KOR) || //3.5 gibt es nur in Korea
        (MajorVersion == 4 && MinorVersion  > 3) || //Es gibt nur 4.0-4.3
        (MajorVersion > 4)) //Es ist keine Version 5.0 oder größer bekannt
    {
        QMessageBox::critical(this, tr("Fehler bei Versionseingabe!"), tr("Leider ist Ihre Version ungültig oder unbekannt und nicht unterstützt.\n"
                                                                          "Bitte prüfen Sie Ihre Eingabe und informieren Sie im Zweifelsfall den Entwickler."));
        return false;

    }

#ifdef Q_OS_WIN32
    //Code für das schlechteste Betriebssystem aller Zeiten (Windoof)
    if (QMessageBox::critical(this, tr("Ihr Betriebssystem ist SCHEISSE!!!"), tr("Sie verwenden ein System, das ich verachte!\nIch empfehle den Umstieg auf beispielsweise eine Linuxdistribution."),
                          tr("Ja, ich weiß, Windoof ist scheiße!"), tr("Windoof ist nicht schlecht!")) != 0)
    {
        QMessageBox::critical(this, tr("Sie sind DÄMLICH!!!!"), tr("Ich HASSE SIE SCHON JETZT! DIESER WIZARD BRICHT JETZT AB!!!"));
        qApp->quit();
        return false;
    }


#endif

    static_cast<UHGPrepWizard*>(wizard())->setVersion(MajorVersion, MinorVersion, region);
    return true;
}

int UHGChooseVersionWizardPage::nextId() const
{
    if ((static_cast<UHGPrepWizard*>(wizard())->getMajorVersion() == 4) &&
            static_cast<UHGPrepWizard*>(wizard())->getMinorVersion() == 3)
        return UHGPrepWizard::Page_Letterbomb;
    else return UHGPrepWizard::Page_Exploits;
}
