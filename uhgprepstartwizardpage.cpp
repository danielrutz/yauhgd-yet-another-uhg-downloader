/*
    Copyright (C) 2012-2014 Daniel Rutz
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    
    Dieses Programm ist freie Software. Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation
    veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß
    Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
    
    Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
    dass es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE,
    sogar ohne die implizite Garantie der MARKTREIFE oder der
    VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der
    GNU General Public License.
    
    Sie sollten ein Exemplar der GNU General Public License zusammen mit
    diesem Programm erhalten haben. Falls nicht, siehe
    <http://www.gnu.org/licenses/>.
    
*/

#include <QVBoxLayout>
#include "uhgprepstartwizardpage.h"
#include "uhgprepwizard.h"

UHGPrepStartWizardPage::UHGPrepStartWizardPage(QWidget *parent) :
    QWizardPage(parent)
{
    //TODO: Nach vWii-Kompatibilität Text anpassen...
    WelcomeLabel = new QLabel(tr("Dieser Assistent wird Ihnen bei der Einrichtung der Daten helfen.\nBitte drücken Sie \"Weiter\", um fortzufahren.\nWARNUNG: NUTZUNG AUF EIGENE GEFAHR! WEDER ICH NOCH SONST WER HAFTET FÜR POTENTIELLE SCHÄDEN.\nEIN SOFTMOD SORGT FÜR ERLÖSCHUNG DER GARANTIE!\nDIESES PROGRAMM IST NICHT WII-U-KOMPATIBEL! NUR ZUR NUTZUNG MIT EINER WII BESTIMMT!"));
    setTitle(tr("Vorbereitung der Daten für den UHG - Start"));
    layout = new QVBoxLayout();
    layout->addWidget(WelcomeLabel);
    setLayout(layout);
}

int UHGPrepStartWizardPage::nextId() const
{
    return UHGPrepWizard::Page_Info;
}
