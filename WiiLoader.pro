#-------------------------------------------------
#
# Project created by QtCreator 2013-03-31T13:01:21
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WiiLoader
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    aboutbox.cpp \
    uhgprepwizard.cpp \
    uhgprepstartwizardpage.cpp \
    uhgprepinfowizardpage.cpp \
    uhgchooseversionwizardpage.cpp \
    uhgprepletterbombwizardpage.cpp \
    uhgprepgameexploitwizardpage.cpp \
    uhgprepcheckinfoswizardpage.cpp \
    uhgprepprocesswizardpage.cpp \ 
    wiiqt/wad.cpp \
    wiiqt/uidmap.cpp \
    wiiqt/u8.cpp \
    wiiqt/tools.cpp \
    wiiqt/tiktmd.cpp \
    wiiqt/sharedcontentmap.cpp \
    wiiqt/settingtxtdialog.cpp \
    wiiqt/savedatabin.cpp \
    wiiqt/savebanner.cpp \
    wiiqt/nusdownloader.cpp \
    wiiqt/nandspare.cpp \
    wiiqt/nanddump.cpp \
    wiiqt/nandbin.cpp \
    wiiqt/md5.cpp \
    wiiqt/lz77.cpp \
    wiiqt/keysbin.cpp \
    wiiqt/ec.cpp \
    wiiqt/bn.cpp \
    wiiqt/blocks0to7.cpp \
    wiiqt/ash.cpp \
    wiiqt/sha1.c \
    wiiqt/aes.c \
    jpatch.cpp

HEADERS  += mainwindow.h \
    aboutbox.h \
    uhgprepwizard.h \
    uhgprepstartwizardpage.h \
    uhgprepinfowizardpage.h \
    uhgchooseversionwizardpage.h \
    uhgprepletterbombwizardpage.h \
    uhgprepgameexploitwizardpage.h \
    uhgprepcheckinfoswizardpage.h \
    uhgprepprocesswizardpage.h \
    wiiqt/wad.h \
    wiiqt/uidmap.h \
    wiiqt/u8.h \
    wiiqt/tools.h \
    wiiqt/tiktmd.h \
    wiiqt/sharedcontentmap.h \
    wiiqt/sha1.h \
    wiiqt/settingtxtdialog.h \
    wiiqt/savedatabin.h \
    wiiqt/savebanner.h \
    wiiqt/nusdownloader.h \
    wiiqt/nandspare.h \
    wiiqt/nanddump.h \
    wiiqt/nandbin.h \
    wiiqt/md5.h \
    wiiqt/lz77.h \
    wiiqt/keysbin.h \
    wiiqt/includes.h \
    wiiqt/ec.h \
    wiiqt/bn.h \
    wiiqt/blocks0to7.h \
    wiiqt/ash.h \
    wiiqt/aes.h \
    jpatch.h \
    JDefs.h

FORMS    += mainwindow.ui \
    aboutbox.ui \
    wiiqt/settingtxtdialog.ui

TRANSLATIONS += languages/WiiLoader_en.ts

OTHER_FILES += \
    LICENSE.txt

CONFIG += c++11
